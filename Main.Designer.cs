﻿namespace BaoJiPlayer
{
    partial class Main
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.btnLoadNewAge = new System.Windows.Forms.Button();
            this.btnLoadPopMsc = new System.Windows.Forms.Button();
            this.wmPlayer = new AxWMPLib.AxWindowsMediaPlayer();
            this.listPlaying = new System.Windows.Forms.ListBox();
            this.gbSetting = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnClrPopMsc = new System.Windows.Forms.Button();
            this.btnClrNewAge = new System.Windows.Forms.Button();
            this.btnAddPopMsc = new System.Windows.Forms.Button();
            this.btnAddNewAge = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.btnStart = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.lbPlayingHours = new System.Windows.Forms.Label();
            this.btnStop = new System.Windows.Forms.Button();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.MainPage_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.About_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.Exit_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listTimeLine = new System.Windows.Forms.ListBox();
            this.btnPause = new System.Windows.Forms.Button();
            this.numSleepHour = new System.Windows.Forms.NumericUpDown();
            this.numPlayHour = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.wmPlayer)).BeginInit();
            this.gbSetting.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSleepHour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPlayHour)).BeginInit();
            this.SuspendLayout();
            // 
            // btnLoadNewAge
            // 
            this.btnLoadNewAge.Location = new System.Drawing.Point(12, 12);
            this.btnLoadNewAge.Name = "btnLoadNewAge";
            this.btnLoadNewAge.Size = new System.Drawing.Size(84, 23);
            this.btnLoadNewAge.TabIndex = 0;
            this.btnLoadNewAge.Text = "加载轻音乐";
            this.btnLoadNewAge.UseVisualStyleBackColor = true;
            this.btnLoadNewAge.Click += new System.EventHandler(this.btnLoadNewAge_Click);
            // 
            // btnLoadPopMsc
            // 
            this.btnLoadPopMsc.Location = new System.Drawing.Point(102, 12);
            this.btnLoadPopMsc.Name = "btnLoadPopMsc";
            this.btnLoadPopMsc.Size = new System.Drawing.Size(87, 23);
            this.btnLoadPopMsc.TabIndex = 1;
            this.btnLoadPopMsc.Text = "加载流行音乐";
            this.btnLoadPopMsc.UseVisualStyleBackColor = true;
            this.btnLoadPopMsc.Click += new System.EventHandler(this.btnLoadPopMsc_Click);
            // 
            // wmPlayer
            // 
            this.wmPlayer.Enabled = true;
            this.wmPlayer.Location = new System.Drawing.Point(514, 327);
            this.wmPlayer.Name = "wmPlayer";
            this.wmPlayer.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("wmPlayer.OcxState")));
            this.wmPlayer.Size = new System.Drawing.Size(487, 329);
            this.wmPlayer.TabIndex = 2;
            this.wmPlayer.PlayStateChange += new AxWMPLib._WMPOCXEvents_PlayStateChangeEventHandler(this.wmPlayer_PlayStateChange);
            // 
            // listPlaying
            // 
            this.listPlaying.DisplayMember = "Name";
            this.listPlaying.FormattingEnabled = true;
            this.listPlaying.ItemHeight = 12;
            this.listPlaying.Items.AddRange(new object[] {
            ""});
            this.listPlaying.Location = new System.Drawing.Point(12, 40);
            this.listPlaying.Name = "listPlaying";
            this.listPlaying.Size = new System.Drawing.Size(496, 616);
            this.listPlaying.TabIndex = 3;
            this.listPlaying.ValueMember = "Value";
            this.listPlaying.DoubleClick += new System.EventHandler(this.listPlaying_DoubleClick);
            // 
            // gbSetting
            // 
            this.gbSetting.Controls.Add(this.numPlayHour);
            this.gbSetting.Controls.Add(this.numSleepHour);
            this.gbSetting.Controls.Add(this.label2);
            this.gbSetting.Controls.Add(this.label1);
            this.gbSetting.Controls.Add(this.btnClrPopMsc);
            this.gbSetting.Controls.Add(this.btnClrNewAge);
            this.gbSetting.Controls.Add(this.btnAddPopMsc);
            this.gbSetting.Controls.Add(this.btnAddNewAge);
            this.gbSetting.Location = new System.Drawing.Point(514, 41);
            this.gbSetting.Name = "gbSetting";
            this.gbSetting.Size = new System.Drawing.Size(487, 116);
            this.gbSetting.TabIndex = 4;
            this.gbSetting.TabStop = false;
            this.gbSetting.Text = "设置";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(237, 88);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(119, 12);
            this.label2.TabIndex = 7;
            this.label2.Text = "连续播放时长(小时):";
            this.label2.DoubleClick += new System.EventHandler(this.label2_DoubleClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 88);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 12);
            this.label1.TabIndex = 4;
            this.label1.Text = "间歇(小时):";
            this.label1.DoubleClick += new System.EventHandler(this.label1_DoubleClick);
            // 
            // btnClrPopMsc
            // 
            this.btnClrPopMsc.Location = new System.Drawing.Point(359, 50);
            this.btnClrPopMsc.Name = "btnClrPopMsc";
            this.btnClrPopMsc.Size = new System.Drawing.Size(106, 23);
            this.btnClrPopMsc.TabIndex = 3;
            this.btnClrPopMsc.Text = "清除流行音乐";
            this.btnClrPopMsc.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClrPopMsc.UseVisualStyleBackColor = true;
            this.btnClrPopMsc.Click += new System.EventHandler(this.btnClrPopMsc_Click);
            // 
            // btnClrNewAge
            // 
            this.btnClrNewAge.Location = new System.Drawing.Point(82, 49);
            this.btnClrNewAge.Name = "btnClrNewAge";
            this.btnClrNewAge.Size = new System.Drawing.Size(117, 23);
            this.btnClrNewAge.TabIndex = 2;
            this.btnClrNewAge.Text = "  清除轻音乐";
            this.btnClrNewAge.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClrNewAge.UseVisualStyleBackColor = true;
            this.btnClrNewAge.Click += new System.EventHandler(this.btnClrNewAge_Click);
            // 
            // btnAddPopMsc
            // 
            this.btnAddPopMsc.Location = new System.Drawing.Point(359, 21);
            this.btnAddPopMsc.Name = "btnAddPopMsc";
            this.btnAddPopMsc.Size = new System.Drawing.Size(106, 23);
            this.btnAddPopMsc.TabIndex = 1;
            this.btnAddPopMsc.Text = "添加流行音乐...";
            this.btnAddPopMsc.UseVisualStyleBackColor = true;
            this.btnAddPopMsc.Click += new System.EventHandler(this.btnAddPopMsc_Click);
            // 
            // btnAddNewAge
            // 
            this.btnAddNewAge.Location = new System.Drawing.Point(82, 20);
            this.btnAddNewAge.Name = "btnAddNewAge";
            this.btnAddNewAge.Size = new System.Drawing.Size(117, 23);
            this.btnAddNewAge.TabIndex = 0;
            this.btnAddNewAge.Text = "添加轻音乐...";
            this.btnAddNewAge.UseVisualStyleBackColor = true;
            this.btnAddNewAge.Click += new System.EventHandler(this.btnAddNewAge_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(358, 11);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(48, 23);
            this.btnStart.TabIndex = 5;
            this.btnStart.Text = "开始";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(856, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(93, 26);
            this.label3.TabIndex = 6;
            this.label3.Text = "播放时长:";
            // 
            // lbPlayingHours
            // 
            this.lbPlayingHours.AutoSize = true;
            this.lbPlayingHours.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPlayingHours.Location = new System.Drawing.Point(948, 10);
            this.lbPlayingHours.Name = "lbPlayingHours";
            this.lbPlayingHours.Size = new System.Drawing.Size(40, 26);
            this.lbPlayingHours.TabIndex = 7;
            this.lbPlayingHours.Text = "0H";
            // 
            // btnStop
            // 
            this.btnStop.Enabled = false;
            this.btnStop.Location = new System.Drawing.Point(460, 11);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(48, 23);
            this.btnStop.TabIndex = 8;
            this.btnStop.Text = "停止";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.notifyIcon1.BalloonTipText = "煲机(第100小时/音量30%,第200小时/音量50%,第300小时/音量70%,第400小时/音量90%)";
            this.notifyIcon1.BalloonTipTitle = "煲机";
            this.notifyIcon1.ContextMenuStrip = this.contextMenuStrip1;
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "煲机";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.DoubleClick += new System.EventHandler(this.notifyIcon1_DoubleClick);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MainPage_ToolStripMenuItem,
            this.toolStripMenuItem1,
            this.About_ToolStripMenuItem,
            this.toolStripMenuItem2,
            this.Exit_ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(101, 82);
            // 
            // MainPage_ToolStripMenuItem
            // 
            this.MainPage_ToolStripMenuItem.Name = "MainPage_ToolStripMenuItem";
            this.MainPage_ToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.MainPage_ToolStripMenuItem.Text = "主页";
            this.MainPage_ToolStripMenuItem.Click += new System.EventHandler(this.MainPage_ToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(97, 6);
            // 
            // About_ToolStripMenuItem
            // 
            this.About_ToolStripMenuItem.Name = "About_ToolStripMenuItem";
            this.About_ToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.About_ToolStripMenuItem.Text = "关于";
            this.About_ToolStripMenuItem.Click += new System.EventHandler(this.About_ToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(97, 6);
            // 
            // Exit_ToolStripMenuItem
            // 
            this.Exit_ToolStripMenuItem.Name = "Exit_ToolStripMenuItem";
            this.Exit_ToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.Exit_ToolStripMenuItem.Text = "退出";
            this.Exit_ToolStripMenuItem.Click += new System.EventHandler(this.Exit_ToolStripMenuItem_Click);
            // 
            // listTimeLine
            // 
            this.listTimeLine.DisplayMember = "Name";
            this.listTimeLine.FormattingEnabled = true;
            this.listTimeLine.ItemHeight = 12;
            this.listTimeLine.Items.AddRange(new object[] {
            "   "});
            this.listTimeLine.Location = new System.Drawing.Point(514, 163);
            this.listTimeLine.Name = "listTimeLine";
            this.listTimeLine.Size = new System.Drawing.Size(487, 160);
            this.listTimeLine.TabIndex = 10;
            this.listTimeLine.ValueMember = "Value";
            // 
            // btnPause
            // 
            this.btnPause.Enabled = false;
            this.btnPause.Location = new System.Drawing.Point(409, 11);
            this.btnPause.Name = "btnPause";
            this.btnPause.Size = new System.Drawing.Size(48, 23);
            this.btnPause.TabIndex = 11;
            this.btnPause.Text = "暂停";
            this.btnPause.UseVisualStyleBackColor = true;
            this.btnPause.Click += new System.EventHandler(this.btnPause_Click);
            // 
            // numSleepHour
            // 
            this.numSleepHour.Enabled = false;
            this.numSleepHour.Location = new System.Drawing.Point(82, 83);
            this.numSleepHour.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numSleepHour.Name = "numSleepHour";
            this.numSleepHour.Size = new System.Drawing.Size(117, 21);
            this.numSleepHour.TabIndex = 8;
            this.numSleepHour.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numSleepHour.KeyUp += new System.Windows.Forms.KeyEventHandler(this.numSleepHour_KeyUp);
            this.numSleepHour.Leave += new System.EventHandler(this.numSleepHour_Leave);
            // 
            // numPlayHour
            // 
            this.numPlayHour.Enabled = false;
            this.numPlayHour.Location = new System.Drawing.Point(359, 82);
            this.numPlayHour.Maximum = new decimal(new int[] {
            24,
            0,
            0,
            0});
            this.numPlayHour.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numPlayHour.Name = "numPlayHour";
            this.numPlayHour.Size = new System.Drawing.Size(106, 21);
            this.numPlayHour.TabIndex = 9;
            this.numPlayHour.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numPlayHour.KeyUp += new System.Windows.Forms.KeyEventHandler(this.numPlayHour_KeyUp);
            this.numPlayHour.Leave += new System.EventHandler(this.numPlayHour_Leave);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1013, 668);
            this.Controls.Add(this.btnPause);
            this.Controls.Add(this.listTimeLine);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.lbPlayingHours);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.gbSetting);
            this.Controls.Add(this.listPlaying);
            this.Controls.Add(this.wmPlayer);
            this.Controls.Add(this.btnLoadPopMsc);
            this.Controls.Add(this.btnLoadNewAge);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(1029, 707);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(1029, 707);
            this.Name = "Main";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "煲机";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Main_FormClosing);
            this.Load += new System.EventHandler(this.Main_Load);
            ((System.ComponentModel.ISupportInitialize)(this.wmPlayer)).EndInit();
            this.gbSetting.ResumeLayout(false);
            this.gbSetting.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numSleepHour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPlayHour)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnLoadNewAge;
        private System.Windows.Forms.Button btnLoadPopMsc;
        private AxWMPLib.AxWindowsMediaPlayer wmPlayer;
        private System.Windows.Forms.ListBox listPlaying;
        private System.Windows.Forms.GroupBox gbSetting;
        private System.Windows.Forms.Button btnClrPopMsc;
        private System.Windows.Forms.Button btnClrNewAge;
        private System.Windows.Forms.Button btnAddPopMsc;
        private System.Windows.Forms.Button btnAddNewAge;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lbPlayingHours;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem MainPage_ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem About_ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem Exit_ToolStripMenuItem;
        private System.Windows.Forms.ListBox listTimeLine;
        private System.Windows.Forms.Button btnPause;
        private System.Windows.Forms.NumericUpDown numPlayHour;
        private System.Windows.Forms.NumericUpDown numSleepHour;
    }
}

