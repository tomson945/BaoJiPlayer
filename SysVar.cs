﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaoJiPlayer
{
    public class SysVar
    {
        /// <summary>
        /// 连续播放时长
        /// </summary>
        public static int PlayingHours = 0;

        /// <summary>
        /// 间歇
        /// </summary>
        public static int SleepHours = 0;

        /// <summary>
        /// 关于窗口是否已经打开
        /// </summary>
        public static bool IsExistAbout = false;
    }
}
