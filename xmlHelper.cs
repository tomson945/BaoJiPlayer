﻿using BaoJiPlayer.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace BaoJiPlayer
{
    public class xmlHelper
    {
        public static List<PlayList> ReadXml(string xmlFile,string mscType)
        {
            List<PlayList> rList = new List<PlayList>();
            try
            {
                //将XML文件加载进来
                XDocument document = XDocument.Load(xmlFile);
                //获取到XML的根元素进行操作
                XElement root = document.Root;
                XElement ele = root.Element(mscType);
                //获取根元素下的所有子元素
                IEnumerable<XElement> enumerable = ele.Elements();
                foreach (XElement item in enumerable)
                {
                    PlayList data = new PlayList();
                    data.Name = item.FirstAttribute.Value;
                    data.File = item.Value;
                    rList.Add(data);
                }
            }
            catch(Exception ex)
            {
            }
            return rList;
        }

        public static void WriteXml(string xmlFile, string mscType,List<PlayList> addList)
        {
            try
            {
                XDocument document = XDocument.Load(xmlFile);
                XElement root = document.Root;
                XElement eleType = root.Element(mscType);

                foreach(PlayList add in addList)
                {
                    XElement eleFile = new XElement("File");

                    eleFile.SetAttributeValue("Name", add.Name);

                    eleFile.SetValue(add.File);

                    eleType.Add(eleFile);
                }

                document.Save(xmlFile);
            }
            catch(Exception ex)
            {

            }
        }

        public static void ClearMscType(string xmlFile, string mscType)
        {
            try
            {
                //将XML文件加载进来
                XDocument document = XDocument.Load(xmlFile);
                //获取到XML的根元素进行操作
                XElement root = document.Root;
                XElement ele = root.Element(mscType);
                ele.RemoveAll();
                document.Save(xmlFile);
            }
            catch (Exception ex)
            {
            }
        }
        public static int SetHour(string xmlFile,int hour)
        {
            int resHour = 0;
            try
            {
                XDocument document = XDocument.Load(xmlFile);
                XElement root = document.Root;

                int hours = int.Parse(root.FirstAttribute.Value);

                hours += hour;

                root.SetAttributeValue("TotHour", hours);

                document.Save(xmlFile);

                resHour = hours;
            }
            catch (Exception ex)
            {
            }
            return resHour;
        }
        public static void SetSleepHour(string xmlFile, int hour)
        {
            try
            {
                XDocument document = XDocument.Load(xmlFile);
                XElement root = document.Root;

                root.SetAttributeValue("SleepHour", hour);

                document.Save(xmlFile);

            }
            catch (Exception ex)
            {
            }
        }
        public static void SetPlayHour(string xmlFile, int hour)
        {
            try
            {
                XDocument document = XDocument.Load(xmlFile);
                XElement root = document.Root;

                root.SetAttributeValue("PlayHour", hour);

                document.Save(xmlFile);

            }
            catch (Exception ex)
            {
            }
        }
        public static P_PlayList Get_PlayListAttr(string xmlFile)
        {
            P_PlayList res = new P_PlayList();
            try
            {
                XDocument document = XDocument.Load(xmlFile);
                XElement root = document.Root;
                IEnumerable<XAttribute> xaList = root.Attributes();

                
                foreach (XAttribute xa in xaList)
                {
                    if (xa.Name.LocalName == "TotHour")
                        res.TotHour = int.Parse(xa.Value);

                    if (xa.Name.LocalName == "SleepHour")
                        res.SleepHour = int.Parse(xa.Value);

                    if (xa.Name.LocalName == "PlayHour")
                        res.PlayHour = int.Parse(xa.Value);
                }
            }
            catch (Exception ex)
            {
            }
            return res;
        }
    }
}
