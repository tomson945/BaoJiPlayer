﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaoJiPlayer.Model
{
    /// <summary>
    /// 播放列表
    /// </summary>
    public class PlayList
    {
        public string Name { get; set; }

        public string File { get; set; }
    }
}
