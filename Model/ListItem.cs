﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BaoJiPlayer.Model
{
    public class ListItem
    {
        //this.listbox1.DisplayMember = "Name";
        //this.listbox1.ValueMember = "Value";
        public string Name { get; set; }

        public string Value { get; set; }

        public ListItem(string Name,string Value)
        {
            this.Name = Name;
            this.Value = Value;
        }
    }
}
